import React, { useEffect } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import './App.css';
import Header from 'component/header/header';
import { useSelector } from 'react-redux';
import { PageError404 } from 'lib/core/page/not-found';
import AutoLog from 'pages/auto-log/auto-log';
import { notification } from 'antd';
import { NotificationPlacement } from 'antd/es/notification/interface';
import { firestore } from 'firebase.config'
import { collection, onSnapshot, query } from 'firebase/firestore';
import { HomeWrapper } from 'pages/home';
import { FilterWrapper } from 'pages/filterpage';
import { DetailWrapper } from 'pages/booking-detail';
import { BookingWrapper } from 'pages/booking';
import { UserDBWrapper } from 'pages/user/dashbord';
import { UserHTRWrapper } from 'pages/user/history';
import { InvoiceWrapper } from 'pages/invoice';
import { SettingUserWrapper, SettingWrapper } from 'pages/user/setting';
import { InvoiceRQWrapper } from 'pages/user/invoice-request';

function App() {
  const { userInf } = useSelector((state: any) => state);
  const [api, contextHolder] = notification.useNotification();

  const openNotification = (placement: NotificationPlacement, value: string) => {
    api.info({
      message: `Trạng thái thanh toán`,
      description: <span>{value}</span>,
      placement,
    });
  };

  useEffect(() => {
    const x = async () => {
      const bookingService = localStorage.getItem('bookingService')
      const messagesRef = collection(firestore, 'realtime');
      const q = query(messagesRef);
      const unsubscribe = onSnapshot(q, (querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const existValue = doc.data()
          if(existValue?.key === 'identifier'){
            if (userInf && userInf.identifier) {
              const checked = existValue?.data === String(userInf.identifier)
              if (checked) {
                openNotification('topLeft', 'Nạp tiền thành công')
              }
            }
          }
          if(existValue?.key === 'transactions'){
            if (bookingService) {
              const parseBooking = JSON.parse(bookingService)
              const checkData = existValue.data.every((transaction: any) =>
                Number(transaction.creditAmount) === Number(parseBooking.price) &&
                String(transaction.description).includes(String(parseBooking.bookingId)))
              if (checkData) {
                openNotification('topLeft', 'Thanh toán thành công')
              }
            }
          }
        });
      });
      return unsubscribe;
    }
    x()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userInf])

  return (
    <BrowserRouter>

      <div className="container">
        <div className="main">
          {contextHolder}
          <Header />
          <Routes>
            <Route path='/' element={<Navigate to='/home' />} />
            <Route path='/user-dashboard' element={<UserDBWrapper />} />
            <Route path='/booking-history' element={<UserHTRWrapper />} />
            <Route path='/booking-detail/:bookingId' element={<DetailWrapper />} />
            <Route path='/invoice/:bookingId' element={<InvoiceWrapper />} />
            <Route path='/settings' element={<SettingWrapper />} />
            <Route path='/setting-user' element={<SettingUserWrapper />} />
            <Route path='/home' element={<HomeWrapper />} />
            <Route path='/filtered' element={<FilterWrapper />} />
            <Route path='/booking' element={<BookingWrapper />} />
            <Route path='/request-invoice' element={<InvoiceRQWrapper />} />
            <Route path='/auto-log' element={<AutoLog />} />
            <Route path='*' element={PageError404()} />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
