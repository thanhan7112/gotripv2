import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyFilter = lazy(() => import('./filtered-list-page'))

export const FilterWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyFilter />
        </Suspense>
    )
}