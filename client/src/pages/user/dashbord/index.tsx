import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyUserDB = lazy(() => import('./user-dashboard'))

export const UserDBWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyUserDB />
        </Suspense>
    )
}