import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyInvoiceRQ = lazy(() => import('./invoice-request'))

export const InvoiceRQWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyInvoiceRQ />
        </Suspense>
    )
}