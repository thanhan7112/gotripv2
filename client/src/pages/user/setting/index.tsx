import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazySetting = lazy(() => import('./setting'))
const LazySettingUser = lazy(() => import('./setting-user'))

export const SettingWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazySetting />
        </Suspense>
    )
}

export const SettingUserWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazySettingUser />
        </Suspense>
    )
}