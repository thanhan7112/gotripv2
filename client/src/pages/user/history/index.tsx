import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyUserHTR = lazy(() => import('./history'))

export const UserHTRWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyUserHTR />
        </Suspense>
    )
}