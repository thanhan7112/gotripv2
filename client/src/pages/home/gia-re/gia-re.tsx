import { Button, Col, Empty, Form, Input, Modal, Row, Select } from "antd";
import axios from "axios";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { CiLocationOn } from "react-icons/ci";
import { useNavigate } from "react-router-dom";
import { dataCountry, mapOption } from "utils/data-country";
import { getAirlineLogo } from "utils/custom/custom-format";
import { LoadingFrame } from "component/loading-frame/loadingFrame";
const { Option } = Select;

dayjs.extend(customParseFormat);

const GiaRe = () => {

    const history = useNavigate()

    // Tạo một mảng chứa 10 tháng tiếp theo
    const nextMonths = [];
    for (let i = 0; i <= 10; i++) {
        const nextMonth = dayjs().add(i, 'month');
        nextMonths.push({
            value: nextMonth.format('DDMMYYYY'),
            label: `Tháng ${nextMonth.format('MM')} năm ${nextMonth.format('YYYY')}`,
        });
    }

    const optionAir = [
        { value: 'All', label: 'Tất cả' },
        { value: 'VJ', label: 'Vietjet Air' },
        { value: 'VN', label: 'Vietnam Airlines' },
        { value: 'QH', label: 'Bamboo Airways' },
        { value: 'VU', label: 'Vietravel Airlines' },
        { value: 'BL', label: 'Pacific Airlines' },
    ]

    const calendarArr: number[] = []

    for (let j = 1; j <= 42; j++) {
        calendarArr.push(j)
    }

    const [adults, setAdults] = useState(1)
    const [children, setChildren] = useState(0)
    const [rooms, setRooms] = useState(0)
    const [openModalFrom, setOpenModalFrom] = useState(false);
    const [openModalTo, setOpenModalTo] = useState(false);
    const [valueInputFrom, setValueInputFrom] = useState('')
    const [valueInputTo, setValueInputTo] = useState('')
    const [listFilter, setListFilter] = useState<any[]>([]);
    const [listFilterTo, setListFilterTo] = useState<any[]>([]);
    const [locationActive, setLocationActive] = useState('Việt Nam')
    const [locationActiveTo, setLocationActiveTo] = useState('Việt Nam')
    const [onchangeValueDepart, setOnchangeValueDepart] = useState('DAD');
    const [onchangeValueToReturn, setOnchangeValueToReturn] = useState('SGN');
    const [twoWay, setTwoWay] = useState(false)
    const [dateSelected, setDateSelected] = useState(nextMonths[0].value)
    const [optionAirSelected, setOptionAirSelected] = useState('ALl')
    const [isLoading, setIsloading] = useState(false)

    const [listCalendar, setListCalendar] = useState<any[]>([])
    const [exlistCalendar, setExListCalendar] = useState<any[]>([])

    const fetchFlightPrice = async () => {
        try {
            if (onchangeValueDepart && onchangeValueToReturn && dateSelected) {
                setIsloading(true)
                const res = await axios.post("http://plugin.datacom.vn/flightmonth", {
                    ProductKey: "r1e0q6z8md6akul",
                    StartPoint: onchangeValueDepart,
                    EndPoint: onchangeValueToReturn,
                    Month: String(dayjs(dateSelected, 'DDMMYYYY').format('MM')),
                    Year: String(dayjs(dateSelected, 'DDMMYYYY').format('YYYY'))
                }, {
                    headers: {}
                });
                setListCalendar(res.data?.ListFare ?? [])
                setExListCalendar(res.data?.ListFare ?? [])
                setIsloading(false)
            }
        } catch (error) {
            // console.error(error)
            setIsloading(false)
        } finally {
            setIsloading(false)
        }
    };

    const flatChildren: any[] = mapOption.flatMap(item => item.children || [])

    useEffect(() => {
        if (valueInputFrom) {
            const filteredData = flatChildren.filter(
                (element) =>
                    element.label.toLowerCase().includes(valueInputFrom.toLowerCase())
                    || element.unsigned.toLowerCase().includes(valueInputFrom.toLowerCase())
                    || element.value.toLowerCase().includes(valueInputFrom.toLowerCase()))
            setListFilter(filteredData)
        } else {
            const filteredData = flatChildren.filter((element) => element.key === locationActive)
            setListFilter(filteredData)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [locationActive, valueInputFrom])

    useEffect(() => {
        if (valueInputTo) {
            const filteredData = flatChildren.filter(
                (element) =>
                    element.label.toLowerCase().includes(valueInputTo.toLowerCase())
                    || element.unsigned.toLowerCase().includes(valueInputTo.toLowerCase())
                    || element.value.toLowerCase().includes(valueInputTo.toLowerCase()))
            setListFilterTo(filteredData)
        } else {
            const filteredData = flatChildren.filter((element) => element.key === locationActiveTo)
            setListFilterTo(filteredData)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [locationActiveTo, valueInputTo])

    useEffect(() => {
        localStorage.removeItem('bookingInf')
    }, [])

    useEffect(() => {
        const defaultValueStr = localStorage.getItem('filterHome');
        if (defaultValueStr) {
            try {
                const defaultValue = JSON.parse(defaultValueStr);
                setOnchangeValueDepart(defaultValue.startPoint);
                setOnchangeValueToReturn(defaultValue.endPoint);
                setAdults(Number(defaultValue.adults));
                setChildren(Number(defaultValue.children));
                setRooms(Number(defaultValue.Inf));
                setTwoWay(defaultValue.twoWay === "true")
            } catch (error) {
                console.error('Error parsing JSON:', error);
                // Handle the error or provide a default value
            }
        }
    }, []);

    const convertOnChangeFrom: string = dataCountry.find((element) => element.code === onchangeValueDepart)?.city ?? ''
    const convertOnChangeTo: string = dataCountry.find((element) => element.code === onchangeValueToReturn)?.city ?? ''

    const handleSelectChangeFrom = (value: string) => {
        setOnchangeValueDepart(value);
        setOpenModalFrom(false);
    };

    const handleSelectChangeTo = (value: string) => {
        setOnchangeValueToReturn(value);
        setOpenModalTo(false);
    };

    const jumpPage = (date: string) => {
        const filters: any = {
            startPoint: onchangeValueDepart,
            endPoint: onchangeValueToReturn,
            adults: String(adults) ?? '',
            children: String(children) ?? '',
            Inf: String(rooms) ?? '',
            departDate: date,
            returnDate: date,
            twoWay: String(twoWay)
        };

        const queryParams = new URLSearchParams(filters).toString();

        const queryString = encodeURIComponent(queryParams)
        localStorage.setItem('filterHome', JSON.stringify(filters))
        history(`/filtered?${queryString}`)
    }

    useEffect(() => {
        if (optionAirSelected === 'All') {
            const filter = exlistCalendar.filter((element) => String(element.Airline))
            setListCalendar(filter)
        } else {
            const filter = exlistCalendar.filter((element) => String(element.Airline) === optionAirSelected)
            setListCalendar(filter)
        }
    }, [optionAirSelected])


    return <section className='list-price'>
        <h1 className='text-20 f-600'>Tìm và đặt vé giá rẻ trong tháng</h1>
        <Row gutter={[0, 12]} className="bd-1" style={{
            width: '100%',
            height:'fit-content'
        }}>
            <Col lg={6} sm={24} xs={24}>
                <div className='searchMenu-loc'>
                    <h4 className='searchMenu__title'>Từ</h4>
                    <Button className='custom-button-location' onClick={() => setOpenModalFrom(true)}>
                        <CiLocationOn /> {convertOnChangeFrom}
                    </Button>
                    <Modal
                        title="Chọn nơi xuất phát"
                        centered
                        open={openModalFrom}
                        onCancel={() => setOpenModalFrom(false)}
                        footer={null}
                    >
                        <div className='modal-form-flex'>
                            <Input
                                placeholder='Nhập tên thành phố hoặc mã sân bay'
                                name='country'
                                type='string'
                                addonBefore={
                                    <Form.Item name="countryCode" noStyle>
                                        <Select defaultValue="Việt Nam" onChange={(value) => setLocationActive(value)}>
                                            {mapOption.map((code) => (
                                                <Option key={String(code.id)} value={code.label}>
                                                    {code.label}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Form.Item>
                                }
                                onChange={(value) => setValueInputFrom(value.target.value)}
                            />
                            <div className='list-item-select'>
                                {
                                    listFilter.length > 0
                                        ? listFilter.map((element) => {
                                            return (
                                                <div className={onchangeValueDepart === element.value ? 'item-select active' : 'item-select'} onClick={() => handleSelectChangeFrom(element.value)}>
                                                    {element.label}
                                                </div>
                                            )
                                        })
                                        : <Empty description={'Không tìm thấy chuyến bay bạn yêu cầu.'} style={{ gridColumn: 'span 4 / span 4' }} />
                                }
                            </div>
                        </div>
                    </Modal>
                </div>
            </Col>
            <Col lg={6} sm={24} xs={24}>
                <div className='searchMenu-loc'>
                    <h4 className='searchMenu__title'>Đến</h4>
                    <Button className='custom-button-location' onClick={() => setOpenModalTo(true)}>
                        <CiLocationOn /> {convertOnChangeTo}
                    </Button>
                    <Modal
                        title="Chọn nơi đến"
                        centered
                        open={openModalTo}
                        onCancel={() => setOpenModalTo(false)}
                        footer={null}
                    >
                        <div className='modal-form-flex'>
                            <Input
                                name='phone'
                                placeholder='Nhập tên thành phố hoặc mã sân bay'
                                type='string'
                                addonBefore={
                                    <Form.Item name="countryCode" noStyle>
                                        <Select defaultValue="Việt Nam" onChange={(value) => setLocationActiveTo(value)}>
                                            {mapOption.map((code) => (
                                                <Option key={String(code.id)} value={code.label}>
                                                    {code.label}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Form.Item>
                                }
                                onChange={(value) => setValueInputTo(value.target.value)}
                            />
                            <div className='list-item-select'>
                                {
                                    listFilterTo.length > 0
                                        ? listFilterTo.map((element) => {
                                            return (
                                                <div className={onchangeValueToReturn === element.value ? 'item-select active' : 'item-select'} onClick={() => handleSelectChangeTo(element.value)}>
                                                    {element.label}
                                                </div>
                                            )
                                        })
                                        : <Empty description={'Không tìm thấy chuyến bay bạn yêu cầu.'} style={{ gridColumn: 'span 4 / span 4' }} />
                                }
                            </div>
                        </div>
                    </Modal>
                </div>
            </Col>
            <Col lg={6} sm={24} xs={24}>
                <div className='searchMenu-loc'>
                    <h4 className='searchMenu__title'>Chọn tháng</h4>
                    <Select
                        className="select-gia-re"
                        defaultValue={nextMonths[0].value}
                        style={{ height: '32px' }}
                        onChange={(value) => setDateSelected(value)}
                        options={nextMonths}
                    />
                </div>
            </Col>
            <Col lg={6} sm={24} xs={24}>
                <div className='searchMenu-loc'>
                    <h4 className='searchMenu__title'>Chọn hãng bay</h4>
                    <Select
                        className="select-gia-re"
                        defaultValue={'All'}
                        style={{ height: '32px' }}
                        onChange={(value) => setOptionAirSelected(value)}
                        options={optionAir}
                    />
                </div>
            </Col>
        </Row>
        <Button onClick={fetchFlightPrice} style={{
            marginLeft: 'auto',
            width: '240px',
            borderRadius: '4px',
            backgroundColor: '#3554d1'
        }} type="primary">Tìm kiếm</Button>
        <div className="table-calendar">
            <div className="calendar-item header">Chủ nhật</div>
            <div className="calendar-item header">Thứ hai</div>
            <div className="calendar-item header">Thứ ba</div>
            <div className="calendar-item header">Thứ tư</div>
            <div className="calendar-item header">Thứ năm</div>
            <div className="calendar-item header">Thứ sáu</div>
            <div className="calendar-item header">Thứ bảy</div>
            {isLoading 
            ? <LoadingFrame gridCol="span 7 / span 7" divWidth={'100%'} divHeight={'568px'} borderRadius={'4px'}/>
            : calendarArr.map((element) => {
                const customDate = listCalendar.find((item) => parseInt(dayjs(String(item.DepartDate), 'DDMMYYY').format('DD'), 10) === element);
                return (
                    customDate ? (
                        <div className="calendar-item active" onClick={() => jumpPage(customDate.DepartDate)}>
                            <div className="flex-row flex-between">
                                <span className="element">{element}</span>
                                {getAirlineLogo(customDate.Airline, '32px')}
                            </div>
                            <span className="text-13 position">{customDate.MinTotalAdtFormat} VND</span>
                        </div>
                    ) : (
                       listCalendar.length > 0 && <div className="calendar-item">{null}</div>
                    )
                );
            })
        }
        </div>

    </section>
}

export default GiaRe