import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyHome = lazy(() => import('./home'))

export const HomeWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyHome />
        </Suspense>
    )
}