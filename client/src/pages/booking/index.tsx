import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyBooking = lazy(() => import('./booking'))

export const BookingWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyBooking />
        </Suspense>
    )
}