import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyInvoice = lazy(() => import('./invoice'))

export const InvoiceWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyInvoice />
        </Suspense>
    )
}