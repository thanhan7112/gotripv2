import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import React, { lazy, Suspense } from 'react';

const LazyDetail = lazy(() => import('./detail'))

export const DetailWrapper = () => {
    return (
        <Suspense fallback={<LoadingSpinner/>}>
            <LazyDetail />
        </Suspense>
    )
}