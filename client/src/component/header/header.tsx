import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { serverHostIO } from 'utils/links/links'
import { IoMdArrowDropdown } from 'react-icons/io'
import './header.css'
import { refreshAccessToken } from 'component/authen';
import { useDispatch, useSelector } from 'react-redux';
import { setOpenModalAuth, setRedirectToLogin, setUserInf, setUserLoginInf } from 'store/reducers';
import axios from 'axios';
import { Avatar, Modal } from 'antd';
import Login from 'component/authen/login/login';
import Signup from 'component/authen/signup/signup';
import LoadingSpinner from 'component/lds-spinner/lds-spinner';
import { UserOutlined } from '@ant-design/icons';

export default function Header() {
    const history = useNavigate();
    const dispatch = useDispatch();
    const { userLoginInf, openSignup, openModalAuth, redirectToLogin } = useSelector((state: any) => state);
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        const checkAuth = async () => {
            try {
                const checked = await refreshAccessToken();
                dispatch(setUserLoginInf(checked))
                dispatch(setRedirectToLogin(false));
            } catch (error) {
                dispatch(setRedirectToLogin(true));
            }
        };

        checkAuth();
    }, [dispatch, history]);

    useEffect(() => {
        const fetchData = async () => {
            const res = await axios.get(`${serverHostIO}/api/get-user-inf/${userLoginInf.userId}`)
            dispatch(setUserInf(res.data))
        }
        if (userLoginInf) {
            fetchData()
        }
    }, [dispatch, redirectToLogin, userLoginInf])

    const handleLogout = async () => {
        try {
            setIsLoading(true)
            const refreshToken = localStorage.getItem('refreshToken')
            await axios.post(`${serverHostIO}/api/logout`, { refreshToken })
            dispatch(setUserLoginInf(null))
            dispatch(setUserInf(null))
            localStorage.removeItem('refreshToken')
            dispatch(setRedirectToLogin(true));
            setIsLoading(false)
            history('/')
        } catch (error) {
            // console.log(error)
        } finally {
            setIsLoading(false)
        }
    };

    useEffect(() => {
        const items: NodeListOf<HTMLDivElement> = document.querySelectorAll(".header__item");
        const user: HTMLDivElement | null = document.querySelector(".fullname");

        const elementArray: HTMLElement[] = Array.from(items);
        if (user instanceof HTMLElement) {
            elementArray.push(user);
        }

        elementArray.forEach(function (item) {
            item.addEventListener("mouseover", function (this: HTMLElement) {
                const listId: string | null = this.getAttribute("data-list");
                if (listId !== null) {
                    const list: HTMLElement | null = document.getElementById(listId);
                    if (list) {
                        list.classList.add("active");
                    }
                }
            });

            item.addEventListener("mouseout", function (this: HTMLElement) {
                const listId: string | null = this.getAttribute("data-list");
                if (listId !== null) {
                    const list: HTMLElement | null = document.getElementById(listId);
                    if (list) {
                        list.classList.remove("active");
                    }
                }
            });
        });
    }, []);

    return (
        <div id="header">
            <div className="header__layout">
                <a className="header__layout-brand" href="/home">
                    <img className="header__layout-logo" src="/media/general/l.png" alt='Logo máy bay' />
                    <div>
                        <h3 className='f-600 logo-text'>SKYO.VN</h3>
                        <p className='label-logo-text'>Your ticket to the skies -</p>
                    </div>
                </a>
                {isLoading && <LoadingSpinner />}
                <Modal footer={null} open={openModalAuth} onCancel={() => dispatch(setOpenModalAuth(false))}>
                    {openSignup === true
                        ? <Signup />
                        : <Login />
                    }
                </Modal>
            </div>
            <ul className="header__layout-right">
                <li className="popup">
                    <span className="header-title">
                        VNĐ
                    </span>
                    <span className="arrow-icon header-title">
                        <IoMdArrowDropdown style={{ fontSize: '24px' }} />
                    </span>
                </li>
                <div className='line'></div>
                <li className="popup display">
                    <Link to={'/booking-history'}>
                        <Avatar size="large" icon={<UserOutlined />} />
                    </Link>
                </li>
                {userLoginInf == null
                    ? <button className='header-button display' onClick={() => dispatch(setOpenModalAuth(true))}>
                        <span className="header-title">
                            Login
                        </span>
                    </button>
                    : <button className='header-button display' onClick={handleLogout}>
                        <span className="header-title">
                            Logout
                        </span>
                    </button>
                }
            </ul>
        </div>
    )
}
