const firebase = require('firebase/firestore');

const { addDoc, arrayUnion, collection, doc, getDocs, onSnapshot, query, updateDoc, where } = require('firebase/firestore');

module.exports = {
  addDoc,
  arrayUnion,
  collection,
  doc,
  getDocs,
  onSnapshot,
  query,
  updateDoc,
  where
};
