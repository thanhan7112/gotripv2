const mongoose = require('mongoose');

const listDbHost = "mongodb+srv://thanhan7112:0201172001An@cluster0.o8suvr6.mongodb.net/?retryWrites=true&w=majority"
const listDb = mongoose.createConnection(listDbHost);

const minPriceSchema = new mongoose.Schema({
    Key: String,
    DepartDate: Number,
    Airline: String,
    MinFareAdt: Number,
    StartPoint: String,
    Month: Number,
    Day: Number,
    Year: Number,
    EndPoint: String,
    MinTotalAdt: Number,
    MinFareAdtFormat: String,
    MinTotalAdtFormat: String,
}, {
    collection: 'minPrice'
});

const MinPrice = listDb.model('minPrice', minPriceSchema);

module.exports = MinPrice;