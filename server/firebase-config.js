const { initializeApp } = require('firebase/app');
const { getDatabase } = require('firebase/database');
const { getFirestore } = require('firebase/firestore');

//Chỉ sử dụng trong quá trình phát triển
const firebaseConfig = {
    apiKey: "AIzaSyCMberzX8w4OmSHUmieE6sqt3pFVS4wxtc",
    authDomain: "realtime-skyo.firebaseapp.com",
    databaseURL: "https://realtime-skyo-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "realtime-skyo",
    storageBucket: "realtime-skyo.appspot.com",
    messagingSenderId: "871733347645",
    appId: "1:871733347645:web:4edb1030805894765a7154",
    measurementId: "G-MQ1FVSG68C",
};

const app = initializeApp(firebaseConfig)
const firestore = getFirestore(app)
const database = getDatabase(app);

module.exports = { database, firestore }